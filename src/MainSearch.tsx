import * as React from 'react';
import './MainSearch.scss';


class MainSearch extends React.Component {


    public constructor(props: object) {
        super(props);
    }

    public render() {
        return (
            <div className="MainSearch">
                <form onSubmit={this.submit}>
                    <input type="text" />
                    <button type="submit">Search</button>
                </form>
            </div>
        );
    }

    private submit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        console.log(this);
    }
}

export default MainSearch;

