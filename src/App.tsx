import * as React from 'react';
import './App.scss';

import axios from "axios";

import MainSearch from "./MainSearch";

interface IPerson {
    firstName: string;
    lastName: string;
};

function personDiv(person: IPerson) {
    return (
        <div>
            <span>{person.firstName} </span>
            <span>{person.lastName}</span>
        </div>
    );
}

class App extends React.Component {


    public componentDidMount() {
        console.log("componentDidMount()");
        axios.get("/query_specs.json")
            .then(response => {
                console.log(response.data);
            })
    }

    public render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img className="App-logo" src="https://insightengines.com/wp-content/themes/insight-engines2018/img/logo.png" alt="Logo" />
                </header>
                {personDiv({
                    firstName: "John",
                    lastName: "Doe"
                })}
                <MainSearch />
            </div>
        );
    }
}

export default App;
